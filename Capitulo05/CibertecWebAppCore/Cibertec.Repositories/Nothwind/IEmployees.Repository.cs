﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Models;

namespace Cibertec.Repositories.Nothwind
{
    public interface IEmployeesRepository: IRepository<Employees>
    {
        Employees GetById(string id);
        bool Update(Employees customer);
        bool Delete(int Id);
    }
}
