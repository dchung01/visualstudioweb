﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Models;


namespace Cibertec.Repositories.Nothwind
{
    public interface ICustomerRepository:IRepository<Customers>
    {
        Customers GetById(string id);
        bool Update(Customers customer);
        bool Delete(String Id);
    }
}
