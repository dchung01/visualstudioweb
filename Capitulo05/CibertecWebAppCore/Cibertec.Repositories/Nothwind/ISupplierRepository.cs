﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Models;

namespace Cibertec.Repositories.Nothwind
{
    public interface ISupplierRepository:IRepository<Suppliers>
    {
        Suppliers GetById(int id);
        int Insert(Suppliers suppliers);
        bool Update(Suppliers suppliers);
        bool Delete(int Id);
    }
}
