﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Models;

namespace Cibertec.Repositories.Nothwind
{
    public interface IUserRepository:IRepository<User>
    {
        User GetById(string id);
        bool Update(User user);
        bool Delete(int id);
    }
}
