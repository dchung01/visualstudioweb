﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Models;

namespace Cibertec.Repositories.Nothwind
{
    public interface IOrderRepository:IRepository<Orders>
    {
        Orders GetById(int id);
        int Insert(Orders orders);
        bool Update(Orders orders);
        bool Delete(int Id);
    }
}
