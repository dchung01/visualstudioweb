﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Models;

namespace Cibertec.Repositories.Nothwind
{
    public interface IProductRepository:IRepository<Products> 
    {
        Products GetById(int id);
        int Insert(Products products);
        bool Update(Products products);
        bool Delete(int Id);
    }
}
