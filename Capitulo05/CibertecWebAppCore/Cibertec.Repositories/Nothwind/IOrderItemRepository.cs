﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Models;

namespace Cibertec.Repositories.Nothwind
{
    public interface IOrderItemRepository: IRepository<OrderItem>
    {
        OrderItem GetById(string id);
        bool Update(OrderItem orderItem);
        bool Delete(int Id);
    }
}
