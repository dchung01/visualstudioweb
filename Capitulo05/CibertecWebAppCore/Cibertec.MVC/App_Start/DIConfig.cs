﻿using SimpleInjector;
using SimpleInjector.Integration.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Cibertec.UnitOfWorks;
using Cibertec.Repositories.Dapper.Northwind;
using System.Reflection;
using System.Web.Mvc;
using SimpleInjector.Integration.Web.Mvc;

namespace Cibertec.MVC.App_Start
{
    public class DIConfig
    {
        public static void ConfigureInjector()
        {
            var Container = new Container();
            Container.Options.DefaultLifestyle = new WebRequestLifestyle();

            Container.Register<IUnitOfWorks>(()=> new NorthwindUnitOfWork(
                ConfigurationManager.ConnectionStrings["NorthWindConnection"].ToString()));

            Container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            Container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(Container));

        }
    }
}