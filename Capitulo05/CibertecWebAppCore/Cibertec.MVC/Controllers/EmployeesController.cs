﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cibertec.UnitOfWorks;
using Cibertec.Repositories.Dapper.Northwind;
using System.Configuration;

namespace Cibertec.MVC.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly IUnitOfWorks _unit;

        public EmployeesController()
        {
            _unit = new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthWindConnection"].ToString());

        }
        public ActionResult Index()
        {
            return View(_unit.employees.getList());
        }
    }
}