﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Cibertec.UnitOfWorks;
using Cibertec.Repositories.Dapper.Northwind;
using System.Configuration;
using Cibertec.Models;

namespace Cibertec.MVC.Controllers
{
    public class CustomerController : Controller
    {
        private readonly IUnitOfWorks _unit;

        public CustomerController(IUnitOfWorks unit)
        {
            //Sin Injeccion de dependencia: _unit = new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthWindConnection"].ToString());

            //Con injeccion de depencia
            _unit = unit;
        }

        // GET: Customer
        public ActionResult Index()
        {
            return View(_unit.customer.getList());
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Customers customer)
        {
            if (ModelState.IsValid)
            {
                _unit.customer.Insert(customer);
                return  RedirectToAction("Index");
            }
            return View(customer);
        }
        public ActionResult Edit(string id)
        {
            return View(_unit.customer.GetById(id));
        }

        [HttpPost]
        public ActionResult Edit(Customers customer)
        {
            if (_unit.customer.Update(customer)) return RedirectToAction("Index");
            {
                return View(customer);
            }
        }

        public ActionResult Delete(String id)
        {
            return View(_unit.customer.GetById(id));
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(String id)
        {
            if (_unit.customer.Delete(id)) return RedirectToAction("Index");
            {
                return View(_unit.customer.GetById(id));
            }
        }
        public ActionResult Details(String id)
        {
            return View(_unit.customer.GetById(id));
        }
    }
}