﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Cibertec.UnitOfWorks;
using Cibertec.Repositories.Dapper.Northwind;
using System.Configuration;
using Cibertec.Models;

namespace Cibertec.MVC.Controllers
{
    public class OrderController : Controller
    {
        private readonly IUnitOfWorks _unit;

        public OrderController(IUnitOfWorks unit)
        {
            //Sin Injeccion de dependencia: _unit = new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthWindConnection"].ToString());

            //Con injeccion de depencia
            _unit = unit;
        }

        // GET: Order
        public ActionResult Index()
        {
            return View(_unit.order.getList());
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Orders orders)
        {
            if (ModelState.IsValid)
            {
                _unit.order.Insert(orders);
                return RedirectToAction("Index");
            }
            return View(orders);
        }
        public ActionResult Edit(int id)
        {
            return View(_unit.order.GetById(id));
        }

        [HttpPost]
        public ActionResult Edit(Orders orders)
        {
            if (_unit.order.Update(orders)) return RedirectToAction("Index");
            {
                return View(orders);
            }
        }

        public ActionResult Delete(int id)
        {
            return View(_unit.order.GetById(id));
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.order.Delete(id)) return RedirectToAction("Index");
            {
                return View(_unit.order.GetById(id));
            }
        }
        public ActionResult Details(int id)
        {
            return View(_unit.order.GetById(id));
        }
    }
}