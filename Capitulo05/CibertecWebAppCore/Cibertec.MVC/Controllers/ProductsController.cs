﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cibertec.UnitOfWorks;
using Cibertec.Repositories.Dapper.Northwind;
using System.Configuration;
using Cibertec.Models;

namespace Cibertec.MVC.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IUnitOfWorks _unit;

        public ProductsController(IUnitOfWorks unit)
        {
            //Sin Injeccion de dependencia: _unit = new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthWindConnection"].ToString());

            //Con injeccion de depencia
            _unit = unit;
        }
        // GET: Products
        public ActionResult Index()
        {
            return View(_unit.product.getList());
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Products products)
        {
            if (ModelState.IsValid)
            {
                _unit.product.Insert(products);
                return RedirectToAction("Index");
            }
            return View(products);
        }
        public ActionResult Edit(int id)
        {
            return View(_unit.product.GetById(id));
        }

        [HttpPost]
        public ActionResult Edit(Products products)
        {
            if (_unit.product.Update(products)) return RedirectToAction("Index");
            {
                return View(products);
            }
        }

        public ActionResult Delete(int id)
        {
            return View(_unit.product.GetById(id));
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.product.Delete(id)) return RedirectToAction("Index");
            {
                return View(_unit.product.GetById(id));
            }
        }
        public ActionResult Details(int id)
        {
            return View(_unit.product.GetById(id));
        }
    }
}