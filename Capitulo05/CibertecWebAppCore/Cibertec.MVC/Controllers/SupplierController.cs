﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cibertec.UnitOfWorks;
using Cibertec.Repositories.Dapper.Northwind;
using System.Configuration;
using Cibertec.Models;

namespace Cibertec.MVC.Controllers
{
    public class SupplierController : Controller
    {
        private readonly IUnitOfWorks _unit;

        public SupplierController(IUnitOfWorks unit)
        {
            //Sin Injeccion de dependencia: _unit = new NorthwindUnitOfWork(ConfigurationManager.ConnectionStrings["NorthWindConnection"].ToString());

            //Con injeccion de depencia
            _unit = unit;
        }
        // GET: SupplierI
        public ActionResult Index()
        {
            return View(_unit.supplier.getList());
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Suppliers suppliers)
        {
            if (ModelState.IsValid)
            {
                _unit.supplier.Insert(suppliers);
                return RedirectToAction("Index");
            }
            return View(suppliers);
        }
        public ActionResult Edit(int id)
        {
            return View(_unit.supplier.GetById(id));
        }

        [HttpPost]
        public ActionResult Edit(Suppliers suppliers)
        {
            if (_unit.supplier.Update(suppliers)) return RedirectToAction("Index");
            {
                return View(suppliers);
            }
        }

        public ActionResult Delete(int id)
        {
            return View(_unit.supplier.GetById(id));
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.supplier.Delete(id)) return RedirectToAction("Index");
            {
                return View(_unit.supplier.GetById(id));
            }
        }
        public ActionResult Details(int id)
        {
            return View(_unit.supplier.GetById(id));
        }
    }
}