﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Cibertec.Repositories.Dapper
{
    public class Repository<T>:IRepository<T> where T:class
    {
        protected string _connectionString;

        public Repository(string connectionString)
        {
            SqlMapperExtensions.TableNameMapper = (type) => { return $"{type.Name}"; };
            _connectionString = connectionString;
        }

        public int Insert(T Entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return (int)connection.Insert(Entity);
            }
        }
        public bool Update(T Entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return (bool)connection.Update(Entity);
            }
        }
        public bool Delete(T Entity)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return (bool)connection.Delete(Entity);
            }
        }
        public T GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Get<T>(id);
            }
        }
        public IEnumerable<T>getList()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<T>();
            }
        }
    }
}
