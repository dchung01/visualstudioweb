﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cibertec.Models;
using Cibertec.Repositories.Nothwind;
using Dapper;
using Dapper.Contrib.Extensions;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class EmployeesRepository:Repository<Employees>,IEmployeesRepository
    {
        public EmployeesRepository(string connectionString) : base(connectionString)
        {

        }

        public Employees GetById(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Employees>().
                    Where(Employees => Employees.EmployeeId.Equals(id)).First();
            }
        }
        public bool update(Employees employees)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                int result = 1;
                return Convert.ToBoolean(result);
            }
        }
        public int insert(Employees employees)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                int result = 1;
                return result;
            }
        }
        public bool Delete(int Id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("delete from Employees " +
                                                "Where EmployeesID=@myID",
                                                new { myID = Id });
                return Convert.ToBoolean(result);
            }
        }
    }
}
