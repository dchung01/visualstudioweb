﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Repositories.Nothwind;
using Cibertec.UnitOfWorks;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class NorthwindUnitOfWork:IUnitOfWorks
    {
        public NorthwindUnitOfWork(string connectionString)
        {
            customer = new CustomerRepository(connectionString);
            employees = new EmployeesRepository(connectionString);

            orderItem = new OrderItemRepository(connectionString);
            order = new OrderRepository(connectionString);
            product = new ProductRepository(connectionString);
            supplier=new SupplierRepository(connectionString);
            user = new UserRepository(connectionString);
        }

        public ICustomerRepository customer
        {
            get;
            private set;
        }
        public IEmployeesRepository employees
        {
            get;
            private set;
        }

        public IOrderItemRepository orderItem
        {
            get;
            private set;
        }
        public IOrderRepository order
        {
            get;
            private set;
        }
        public IProductRepository product
        {
            get;
            private set;
        }
        public ISupplierRepository supplier
        {
            get;
            private set;
        }
        public IUserRepository user
        {
            get;
            private set;
        }

    }

}
