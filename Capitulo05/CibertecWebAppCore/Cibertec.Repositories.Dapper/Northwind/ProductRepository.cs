﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Cibertec.Models;
using Cibertec.Repositories.Nothwind;

using Dapper;
using Dapper.Contrib.Extensions;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class ProductRepository:Repository<Products>,IProductRepository
    {
        public ProductRepository(string connectionString) : base(connectionString)
        {

        }
        public Products GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Products>().
                    Where(Products => Products.ProductID.Equals(id)).First();
            }
        }

        public bool Update(Products products)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("UPDATE [dbo].[Products] SET " +
                                                "[ProductName]=@ProductName" +
                                                ",[SupplierId]=@SupplierId" +
                                                ",[CategoryId]=@CategoryId " +
                                                ",[UnitPrice]=@UnitPrice" +
                                                ",[Discontinued]=@Discontinued " +
                                                "Where ProductID=@ProductId",
                                                new
                                                {
                                                    ProductName = products.ProductName,
                                                    SupplierId = products.SupplierId,
                                                    CategoryId = products.CategoryId,
                                                    UnitPrice = products.UnitPrice,
                                                    Discontinued = products.Discontinued,
                                                    ProductId=products.ProductID
                                                });
                return Convert.ToBoolean(result);
            }
        }
        public int Insert(Products products)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("INSERT INTO [dbo].[Products] ([ProductName] " +
                                                ",[SupplierId],[CategoryId] " +
                                                ",[UnitPrice],[Discontinued])" +
                                                "VALUES (@ProductName,@SupplierId,@CategoryId" +
                                                ",@UnitPrice,@Discontinued)",
                                                new
                                                {
                                                    ProductName = products.ProductName,
                                                    SupplierId = products.SupplierId,
                                                    CategoryId = products.CategoryId,
                                                    UnitPrice = products.UnitPrice,
                                                    Discontinued = products.Discontinued,

                                                });
                return result;
            }
        }
        public bool Delete(int Id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("delete from Products " +
                                                "Where ProductID=@myID",
                                                new { myID = Id });
                return Convert.ToBoolean(result);
            }
        }
    }
}
