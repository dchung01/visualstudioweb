﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Models;
using Cibertec.Repositories.Nothwind;

using Dapper;
using Dapper.Contrib.Extensions;

namespace Cibertec.Repositories.Dapper
{
    public class CustomerRepository:Repository<Customers>,ICustomerRepository
    {
        public CustomerRepository(string connectionString) : base(connectionString)
        {

        }

        public Customers GetById(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Customers>().
                    Where(Customer => Customer.CustomerId.Equals(id)).First();
            }
        }
        public bool Update(Customers customer)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("Update Customers " +
                                      "set CompanyName=@company,"+
                                      "ContactName=@contact,"+
                                      "City=@city,"+
                                      "Country=@country,"+
                                      "Phone=@phone "+
                                      "Where CustomerID=@myID",
                                      new
                                      {
                                          company=customer.CompanyName,
                                          contact=customer.ContactName,
                                          city=customer.City,
                                          country=customer.Country,
                                          phone=customer.Phone,
                                          myID=customer.CustomerId
                                      });
                return Convert.ToBoolean(result);
            }
        }
        public bool Delete(String Id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("delete from customers " +
                                                "Where CustomerID=@myID",
                                                new { myID=Id});
                return Convert.ToBoolean(result);
            }
        }
    }
}
