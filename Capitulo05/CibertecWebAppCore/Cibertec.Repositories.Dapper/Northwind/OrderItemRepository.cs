﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using Cibertec.Models;
using Cibertec.Repositories.Nothwind;

using Dapper;
using Dapper.Contrib.Extensions;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class OrderItemRepository: Repository<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(string connectionString) : base(connectionString)
        {

        }
        public OrderItem GetById(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<OrderItem>().
                    Where(OrderItem => OrderItem.OrderId.Equals(id)).First();
            }
        }
        public bool Update(OrderItem orderItem)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("Update [Order Details] " +
                                      "set ProductID=@ProdId," +
                                      "UnitPrice=@UnitP," +
                                      "Quantity=@Quantity," +
                                      "Discount=@Discount" +
                                      "Where OrderId=@myID",
                                      new
                                      {
                                          ProdId = orderItem.ProductId,
                                          UnitP = orderItem.UnitPrice,
                                          Quantity = orderItem.Quantity,
                                          Discount = orderItem.Discount,
                                          myID = orderItem.OrderId
                                      });
                return Convert.ToBoolean(result);
            }
        }
        public bool Delete(int Id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("delete from OrderItem " +
                                                "Where EmployeesID=@myID",
                                                new { myID = Id });
                return Convert.ToBoolean(result);
            }
        }
    }
}
