﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using Cibertec.Models;
using Cibertec.Repositories.Nothwind;

using Dapper;
using Dapper.Contrib.Extensions;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class SupplierRepository:Repository<Suppliers>,ISupplierRepository
    {
        public SupplierRepository(string connectionString) : base(connectionString)
        {

        }
        public Suppliers GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Suppliers>().
                    Where(Suppliers => Suppliers.SupplierId.Equals(id)).First();
            }
        }
        public int Insert(Suppliers suppliers)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("INSERT INTO [dbo].[Suppliers] ([CompanyName] " +
                                                ",[ContactName],[ContactTitle],[City],[Country] " +
                                                ",[Phone],[Fax])" +
                                                "VALUES (@CompanyName,@ContactName,@ContactTitle,@City,@Country" +
                                                ",@Phone,@Fax)",
                                                new
                                                {
                                                    CompanyName = suppliers.CompanyName,
                                                    ContactName = suppliers.ContactName,
                                                    ContactTitle = suppliers.ContactTitle,
                                                    City = suppliers.City,
                                                    Country = suppliers.Country,
                                                    Phone = suppliers.Phone,
                                                    Fax = suppliers.Fax,
                                                });
                return result;
            }
        }
        public bool Update(Suppliers suppliers)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("UPDATE [dbo].[Suppliers] SET " +
                                                "[CompanyName]=@CompanyName" +
                                                ",[ContactName]=@ContactName" +
                                                ",[ContactTitle]=@ContactTitle " +
                                                ",[City]=@City" +
                                                ",[Country]=@Country " +
                                                ",[Fax]=@Fax " +
                                                ",[Phone]=@Phone " +
                                                "Where SupplierID=@SupplierID",
                                                new
                                                {
                                                    CompanyName = suppliers.CompanyName,
                                                    ContactName = suppliers.SupplierId,
                                                    ContactTitle = suppliers.ContactTitle,
                                                    City = suppliers.City,
                                                    Country = suppliers.Country,
                                                    Phone = suppliers.Phone,
                                                    Fax = suppliers.Fax,
                                                    SupplierID=suppliers.SupplierId
                                                });
                return Convert.ToBoolean(result);
            }
        }
        public bool Delete(int Id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("delete from [Suppliers] " +
                                                "Where SupplierID=@myID",
                                                new { myID = Id });
                return Convert.ToBoolean(result);
            }
        }
    }
}
