﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using Cibertec.Models;
using Cibertec.Repositories.Nothwind;

using Dapper;
using Dapper.Contrib.Extensions;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class OrderRepository:Repository<Orders>, IOrderRepository
    {
        public OrderRepository(string connectionString) : base(connectionString)
        {

        }
        public Orders GetById(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Orders>().
                    Where(Orders => Orders.OrderId.Equals(id)).First();
            }
        }
        public int Insert(Orders orders)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("INSERT INTO [dbo].[Orders] ([CustomerID] " +
                                                ",[EmployeeID],[OrderDate] " +
                                                ",[ShippedDate],[ShipVia],[Freight],[ShipName])" +
                                                "VALUES (@CustomerID,@EmployeeID,@OrderDate" +
                                                ",@ShippedDate,@ShipVia,@Freight,@ShipName)",
                                                new
                                                {
                                                    CustomerID = orders.CustomerId,
                                                    EmployeeID=orders.EmployeeId,
                                                    OrderDate=orders.OrderDate,
                                                    ShippedDate=orders.ShippedDate,
                                                    ShipVia=orders.SHipVia,
                                                    Freight=orders.Freight,
                                                    ShipName=orders.ShipName
                                                });
                return result;
            }
        }
        public bool Update(Orders orders)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("UPDATE [dbo].[Orders] SET "+
                                                "[CustomerID]=@CustomerID" +
                                                ",[EmployeeID]=@EmployeeID" +
                                                ",[OrderDate]=@OrderDate " +
                                                ",[ShippedDate]=@ShippedDate" +
                                                ",[ShipVia]=@ShipVia," +
                                                "[Freight]=@Freight," +
                                                "[ShipName]=@ShipName " +
                                                 "Where OrderID=@OrderID",
                                                new
                                                {
                                                    CustomerID = orders.CustomerId,
                                                    EmployeeID = orders.EmployeeId,
                                                    OrderDate = orders.OrderDate,
                                                    ShippedDate = orders.ShippedDate,
                                                    ShipVia = orders.SHipVia,
                                                    Freight = orders.Freight,
                                                    ShipName = orders.ShipName,
                                                    OrderID=orders.OrderId
                                                });
                return Convert.ToBoolean(result);
            }
        }
        public bool Delete(int Id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("delete from Orders " +
                                                "Where OrderId=@myID",
                                                new { myID = Id });
                return Convert.ToBoolean(result);
            }
        }
    }
}
