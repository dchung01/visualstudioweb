﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using Cibertec.Models;
using Cibertec.Repositories.Nothwind;

using Dapper;
using Dapper.Contrib.Extensions;

namespace Cibertec.Repositories.Dapper.Northwind
{
    public class UserRepository:Repository<User>,IUserRepository
    {
        public UserRepository(string connectionString) : base(connectionString)
        {

        }
        public User GetById(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<User>().
                    Where(User => User.email.Equals(id)).First();
            }
        }
        public bool update(User user)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                int result = 1;
                return Convert.ToBoolean(result);
            }
        }
        public bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("delete from User " +
                                                "Where email=@myID",
                                                new { myID = id });
                return Convert.ToBoolean(result);
            }
        }
    }
}
