﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cibertec.Repositories.Nothwind;

namespace Cibertec.UnitOfWorks
{
    public interface IUnitOfWorks
    {
         ICustomerRepository customer { get; }
         IEmployeesRepository employees { get;}

         IOrderItemRepository orderItem { get; }
         IProductRepository product { get; }
         ISupplierRepository supplier { get; }
         IUserRepository user { get; }
         IOrderRepository order { get; }
    }
}
