﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cibertec.Models
{
    public class Employees
    {
        public int id { get; set; }
        public string EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string City { get; set; }
        public int ReportsTo { get; set; }
    }
}
