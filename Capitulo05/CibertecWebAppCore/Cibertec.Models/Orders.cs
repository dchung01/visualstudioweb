﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cibertec.Models
{
    public class Orders
    {
        public int id { get; set; }
        public int OrderId { get; set; }
        public string CustomerId { get; set; }
        public int EmployeeId { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime ShippedDate { get; set; }
        public int SHipVia { get; set; }
        public decimal? Freight { get; set; } 
        public string ShipName { get; set; }
    }
}
